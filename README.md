# Overview

Kubernates flexvolume plugin for CephFS-FUSE

## Requirements

The flexvolume needs the following packages installed:

* ceph-fuse
* jq

In addition, the CephFS configuration shoulbe stored in `/etc/ceph/ceph.conf`

## Installation

The plugin has to be copied into `/usr/libexec/kubernetes/kubelet-plugins/volume/exec/cern~cephfs/`

## Usage

The flexvolume needs three arguments to be able to mount a CephFS share:

* `share`: CephFS share, e.g. `/volumes/_nogroup/3a2a682c-5645-4ff0-826e-d2a3a9b456c1`
* `authid`: Authentication identifier for the share e.g. `yourauthid01`
* `keyring`: A secret (named `keyring`) containing a valid keyring to access the share

The keyring is a multiline text file looking like this (myauthid is the above authid value, and myaccesskey a base64 
string returned by `manila access-list`):
```
[client.myauthid]
    key = myaccesskey
```

### Example

Assuming the keyring is in a file named `example.keyring`, first create the secret
in your project:

```
oc create secret generic keyring --from-file=keyring=example.keyring --type=cern/cephfs
```
Important! Both the secret name and the key inside the secret has to be named `keyring`.
Running the command above will guarantee this requirement.

Once done, add the volume to your application. The volume section should be:

```
...
  flexVolume:
    driver: cern/cephfs
    options:
      authid: yourauthid01
      share: /volumes/_nogroup/3a2a682c-5645-4ff0-826e-d2a3a9b456c1
    secretRef:
      name: keyring
...
```
The keyring file can also be specified directly in the flexvolume options with base64 encoding

```
...
flexVolume:
  driver: cern/cephfs
  options:
    authid: yourauthid01
    share: /volumes/_nogroup/3a2a682c-5645-4ff0-826e-d2a3a9b456c1
    keyring: W2NsaWBXXXXXXXXXX==
...
```
Replace `authid` and `share` with the values of the share you want to mount.

### Deployment

This is deployed into openshift.cern.ch and openshift-dev.cern.ch using Puppet to synchronize
the flexvolume into the corresponding folder used by Kubernetes/Openshift.
